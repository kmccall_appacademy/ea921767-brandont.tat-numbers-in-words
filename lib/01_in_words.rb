ONES = {
  0 => "zero", 1 => "one", 2 => "two",
  3 => "three", 4 => "four", 5 => "five",
  6 => "six", 7 => "seven", 8 => "eight",
  9 => "nine"
}

TEENS ={
  10 => "ten", 11 => "eleven", 12 => "twelve",
  13 => "thirteen", 14 => "fourteen", 15 => "fifteen",
  16 => "sixteen", 17 => "seventeen", 18 => "eighteen",
  19 => "nineteen"
}

TENS = {
  20 => "twenty", 30 => "thirty", 40 => "forty",
  50 => "fifty", 60 => "sixty", 70 => "seventy",
  80 => "eighty", 90 => "ninety"
}

POWERS_TEN = {
  100 => "hundred", 1000 => "thousand",
  1_000_000 => "million",
  1_000_000_000 => "billion",
  1_000_000_000_000 => "trillion"
}

class Fixnum
  def initialize(value)
    @value = value
  end

  def get_powers_ten
    POWERS_TEN.keys.select{|val| val <= self}.max
  end

  def in_words
    if self < 10
      return ONES[self]
    elsif self < 20
      return TEENS[self]
    elsif self < 100
      return TENS[self] if TENS.has_key?(self)
      ones = self%10
      return "#{TENS[self-ones]} #{ONES[ones]}"
    else
      powers_ten = get_powers_ten
      above_100 = "#{(self/powers_ten).in_words} #{POWERS_TEN[powers_ten]}"
      if self%powers_ten == 0
        return above_100
      else
        return "#{above_100} #{(self%powers_ten).in_words}"
      end

    end
  end
end
